/* 
 * Auth Service
 * 
 */
var Service = require('../service'),
	Endpoint = require('../endpoint'),
	Query = require('../query');

module.exports = Service.extend({
	endpoints: {
		
		// Initial Registration
		'register/initial': new Endpoint('POST', function(req, res) {
			var self = this,
				username = req.body.username,
				email = req.body.email;
			
			// Verify body
			if(!username) return res.sendError('No username in request', 'BAD_REQUEST', 400);
			if(!email) return res.sendError('No email in request', 'BAD_REQUEST', 400);
			
			// Register the user
			self.api.authProvider.initialRegister({
				username: username,
				email: email
			}, req.session).then(function() {
				res.status(200).json({});
			}).fail(function(err) {
				res.sendError(String(err), 'BAD_REQUEST', 400);
			});
		})
		
	}
});