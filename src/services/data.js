/* 
 * Data Service (/data)
 * 
 */
var Service = require('../service'),
	SecureEndpoint = require('../secure-endpoint'),
	Query = require('../query'),
	EndpointException = require('../exceptions/endpoint-exception'),
	q = require('q');

module.exports = Service.extend({
	endpoints: {
		'describe': new SecureEndpoint('GET', function(req, res) {
			var self = this;
			
			return req.session.describe().then(function(description) { // Remove the user and session types
				var typeList = description.types,
					typeCount = typeList.length,
					type;
				
				// Skip this if no auth provider is set
				if(self.api.authProvider === null) return typeList;
				
				for(var index=0; index<typeCount; index++) {
					type = typeList[index];
					if(self.service._isValidType(type, self.api) === false) {
						typeList.splice(index, 1);
						index--;
						typeCount--;
					}
				}
				
				return typeList;
			}).then(function(typeList) { // Respond
				res.json({
					types: typeList
				});
				
			});
		}),
		
		'records/:typeName': new SecureEndpoint('POST', function(req, res) {
			var self = this,
				type = req.params.typeName;
			
			return q.fcall(function() { // Verify type
				if(self.service._isValidType(type, self.api) === false) {
					throw new Error('Access to that type is not allowed');
				}
				
			}).then(function() { // Insert the data
				return req.session.create(type, req.body);
				
			}).then(function(inserted) { // Respond
				res.json(inserted);
				
			}).fail(function(err) {
				res.sendError(String(err), 'BAD_REQUEST', 400);
			});
		}),
		
		'records/:getType': new SecureEndpoint('GET', function(req, res) {
			var pageQuery = new Query(req.params.getType, {});
			
			return req.session.readGeneric(pageQuery).then(function(records) {
				res.json({
					records: records,
					hasMore: false
				});
			});
		})
	},
	
	_isValidType: function(type, api) {
		if(type === api.authProvider.userType || type === api.authProvider.sessionType) {
			return false;
		} else {
			return true;
		}
	}
});