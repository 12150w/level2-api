/* 
 * Data Session
 * 
 */
var Class = require('./base/class'),
	Query = require('./query'),
	Level2Exception = require('./exceptions/level2-exception'),
	SharingOption = require('./sharing-option'),
	q = require('q');

module.exports = Class.extend({
	_user: null,
	_adapter: null,
	
	init: function(user, adapter) {
		this._user = user;
		this._adapter = adapter;
		
		this.isPublic = false;
	},
	
	create: function(typeName, data) {
		var inserted = q.defer();
		var adapter = this._adapter;
		
		//TODO: Get the default sharing options
		var options = [
			new SharingOption(
				SharingOption.type.USER,
				SharingOption.level.ADMIN,
				this._user.id
			)
		];
		
		// Commit the data
		var passBackData;
		adapter.addSharingBefore(typeName, options, data).then(function(secureData) {
			return adapter.commit(typeName, secureData);
		}).then(function(insertedData) {
			passBackData = insertedData;
			return adapter.addSharingAfter(typeName, options, insertedData);
		}).then(function() {
			inserted.resolve(passBackData);
		}).fail(inserted.reject);
		
		return inserted.promise;
	},
	
	read: function(typeName, query, options) {
		options = options || {};
		
		// Add sharing to filter if needed
		var addSharingPromise;
		if(options.disableSecurity === true) addSharingPromise = q.fcall(function() {});
		else addSharingPromise = this._adapter.addSharingToQuery(typeName, query, this._user);
		
		return addSharingPromise.then(function(secureQuery) {
			if(options.disableSecurity === true) secureQuery = query;
			return this._adapter.extract(typeName, secureQuery);
		}.bind(this));
	},
	
	readGeneric: function(query) {
		if(query instanceof Query === false) {
			return q.fcall(function() { throw new Level2Exception('BAD_ARG', 'query must be a Query instance'); });
		}
		
		return this.read(
			query.getTypeName(),
			this._adapter.convertQuery(query),
			query.getOptions()
		);
	},
	
	update: function(typeName, id, data) {
		return this._getSecureLevel(typeName, id).then(function(secureLevel) {
			if(secureLevel < 2) throw new Level2Exception('UNAUTHORIZED', 'This user does not have write access to the record');
			data.id = id;
			return this._adapter.update(typeName, data);
		}.bind(this));
	},
	
	delete: function(typeName, id) {
		return this.destroy(typeName, id);
	},
	
	destroy: function(typeName, id) {
		return this._getSecureLevel(typeName, id).then(function(secureLevel) {
			if(secureLevel < 3) throw new Level2Exception('UNAUTHORIZED', 'This user does not have delete access to the record');
			return this._adapter.destroy(typeName, id);
		}.bind(this));
	},
	
	describe: function() {
		var description = {};
		
		return this._adapter.getTypes().then(function(types) { // Load the types
			description.types = types;
		}).then(function() {
			return description;
		});
	},
	
	_getSecureLevel: function(typeName, id) {
		var verifyQuery = new Query(typeName, {id: id}, {disableSecurity: true, limit: 1});
		
		return this.readGeneric(verifyQuery).then(function(foundRecords) {
			if(foundRecords.length !== 1) throw new Level2Exception('NOT_FOUND', 'No ' + typeName + ' found with id ' + id);
			return this._adapter.getSecurityLevel(foundRecords[0], this._user);
		}.bind(this));
	}
	
});