/* 
 * Service
 * 
 */
var Class = require('./base/class'),
	Level2Exception = require('./exceptions/level2-exception'),
	Endpoint = require('./endpoint');

module.exports = Class.extend({
	endpoints: {},
	
	init: function() {
		// These are set after _register is called
		this._api = null;
	},
	
	_register: function(basePath, api, app) {
		var endpointData = {};
		basePath = basePath === '/' ? '' : basePath;
		this._api = api;
		
		// Add user endpoints
		var endpointPath, endpoint;
		for(var partialPath in this.endpoints) {
			endpoint = this.endpoints[partialPath];
			endpointPath = basePath + '/' + (partialPath === '/' ? '' : partialPath);
			endpointData[partialPath] = endpointPath;
			
			// Set up the handler
			if(endpoint instanceof Endpoint === false) {
				if(typeof endpoint !== 'function') throw new Level2Exception('BAD_ARG', 'endpoint handlers must be either Endpoint instances or functions');
				endpoint = new Endpoint(endpoint);
			}
			
			// Register the handler
			endpoint.api = this._api;
			endpoint.fullPath = endpointPath;
			endpoint.service = this;
			if(endpoint._method === null) {
				app.use(endpointPath, this._endpointRunner.bind(endpoint));
			} else {
				app[endpoint._method](endpointPath, this._endpointRunner.bind(endpoint));
			}
		}
		
		// Add standard service root endpoint (if not overridden)
		if(this.endpoints['/'] === undefined) {
			app.get(basePath + '/', function(req, res) {
				res.json({
					endpoints: endpointData
				});
			});
		}
	},
	
	_endpointRunner: function(req, res, next) {
		// NOTE: 'this' is set to the endpoint to run
		var endpoint = this;
		
		// Run the endpoint
		endpoint.preProcess(req, res, next).fail(function(err) {
			res.sendError('Unable to process request: ' + err, 'CANCELLED', err.statusCode || 400);
		}).then(function() {
			try {
				var possiblePromise = endpoint.processRequest(req, res, next);
				if(!!possiblePromise && typeof(possiblePromise.fail) === 'function') {
					possiblePromise.fail(function(err) {
						endpoint.service._sendEndpointError(err, res);
					});
				}
			} catch(err) {
				endpoint.service._sendEndpointError(err, res);
			}
		}).done();
	},
	
	_sendEndpointError: function(err, res) {
		res.sendError(String(err), err.code || 'EXCEPTION', err.statusCode || 500, {
			trace: err.stack
		});
	}
});