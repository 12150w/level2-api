module.exports.Class = require('./base/class');

var exceptions = require('./exceptions');
module.exports.Level2Exception = exceptions.Level2Exception;
module.exports.APIException = exceptions.APIException;
module.exports.EndpointException = exceptions.EndpointException;

module.exports.DataSource = require('./data-source');
module.exports.DataAdapter = require('./data-adapter');
module.exports.DataSession = require('./data-session');
module.exports.SharingOption = require('./sharing-option');
module.exports.API = require('./api');
module.exports.Service = require('./service');
module.exports.AuthProvider = require('./auth-provider');
module.exports.Query = require('./query');
module.exports.Endpoint = require('./endpoint');
module.exports.SecureEndpoint = require('./secure-endpoint');