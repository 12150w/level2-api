/* 
 * AuthProvider
 * 
 */
var Class = require('./base/class'),
	Level2Exception = require('./exceptions/level2-exception'),
	Query = require('./query'),
	q = require('q'),
	crypto = require('crypto'),
	bcrypt = require('bcrypt');

module.exports = Class.extend({
	userType: 'l2_user',
	sessionType: 'l2_session',
	sessionPeriod: 60*60*2,
	
	login: function(identity, dataSession) {
		var self = this,
			loginUser;
		
		return q.fcall(function() { // Verify the identity is complete
			if(!identity) throw new Level2Exception('BAD_ARG', 'identity can not be blank');
			if(!identity.username) throw new Level2Exception('BAD_ARG', 'identity username can not be blank');
			if(!identity.password) throw new Level2Exception('BAD_ARG', 'identity password can not be blank');
			
		}).then(function() { // Find the user by username
			var loadUserQuery = new Query(self.userType, {username: identity.username}, {limit: 1, disableSecurity: true});
			return dataSession.readGeneric(loadUserQuery);
			
		}).then(function(found) { // Compare the password
			if(found.length < 1) {
				throw new Level2Exception('UNAUTHORIZED', 'Invalid username or password');
			}
			loginUser = found[0];
			return q.nfcall(bcrypt.compare, identity.password, loginUser.password);
			
		}).then(function(validPassword) { // Create the session
			if(validPassword !== true) {
				throw new Level2Exception('UNAUTHORIZED', 'Invalid username or password');
			}
			return self._createSession(loginUser.id, dataSession);
		}).then(function(session) {
			return session.key;
		});
	},
	
	logout: function(sessionId, dataSession) {
		var self = this,
			logoutTime = new Date();
		
		return q.fcall(function() { // Verify arguments
			if(!sessionId) throw new Level2Exception('BAD_ARG', 'sessionId can not be null');
			var findSessionQuery = new Query(
				self.sessionType,
				{key: sessionId},
				{limit: 1, disableSecurity: true}
			);
			return dataSession.readGeneric(findSessionQuery);
			
		}).then(function(foundSessions) { // Close out the session, if one is found
			if(foundSessions.length < 1) throw new Level2Exception('NOT_FOUND', 'no active or expired session can be found with the key ' + key);
			var logoutSession = foundSessions[0];
			
			if(logoutSession.invalidAfter < logoutTime) {
				return;
			} else {
				return dataSession.update(self.sessionType, logoutSession.id, {invalidAfter: logoutTime});
			}
		});
	},
	
	validate: function(sessionId, dataSession) {
		var self = this;
		
		return q.fcall(function() { // Verify the arguments are valid
			if(!sessionId) throw new Level2Exception('BAD_ARG', 'sessionId can not be blank');
			
		}).then(function() { // Lookup a session
			var sessionQuery = new Query(
				self.sessionType,
				{ key: sessionId },
				{
					limit: 1,
					greaterThan: {invalidAfter: new Date()},
					disableSecurity: true
				}
			);
			return dataSession.readGeneric(sessionQuery);
			
		}).then(function(foundSessions) { // Update the existing session
			if(foundSessions.length < 1) {
				var err = new Level2Exception('UNAUTHORIZED', 'Session has expired');
				err.statusCode = 401;
				throw err;
			}
			var validSession = foundSessions[0];
			
			// Queue the session expire update
			dataSession.update(self.sessionType, validSession.id, {
				invalidAfter: self._getCurrentExpiration()
			});
			
			var loadUserQuery = new Query(self.userType, {id: validSession.user}, {limit: 1, disableSecurity: true});
			return dataSession.readGeneric(loadUserQuery);
			
		}).then(function(foundUsers) { // Prepare the user for client safe access
			var sessionUser = JSON.parse(JSON.stringify(foundUsers[0]));
			delete sessionUser.registrationKey;
			delete sessionUser.password;
			
			return sessionUser;
		});
	},
	
	initialRegister: function(userInfo, dataSession) {
		// Check for not null values
		if(!userInfo.username) {
			return q.fcall(function() { throw new Level2Exception('BAD_ARG', 'userInfo.username can not be null'); });
		}
		if(!userInfo.email) {
			return q.fcall(function() { throw new Level2Exception('BAD_ARG', 'userInfo.email can not be null'); });
		}
		
		//TODO: validate email and username format
		
		// Look up for an existing user
		var existingUsernameQuery = new Query(this.userType, {username: userInfo.username}, {limit: 1, disableSecurity: true});
		
		return dataSession.readGeneric(existingUsernameQuery).then(function(foundUsers) {
			if(foundUsers.length > 0) throw new Level2Exception('BAD_USERNAME', 'The username ' + userInfo.username + ' is already in use');
			
			var existingEmailQuery = new Query(this.userType, {email: userInfo.email}, {limit: 1, disableSecurity: true});
			return dataSession.readGeneric(existingEmailQuery);
		}.bind(this)).then(function(foundUsers) {
			if(foundUsers.length > 0) throw new Level2Exception('BAD_EMAIL', 'An account already exists for the email ' + userInfo.email);
			
			return q.nfcall(crypto.randomBytes, 20);
		}).then(function(rawKey) {
			var expirationDate = new Date();
			expirationDate.setDate(expirationDate.getDate() + 7);
			
			// Insert the user with registration key
			return dataSession.create(this.userType, {
				username: userInfo.username,
				email: userInfo.email,
				registrationKey: rawKey.toString('hex').toUpperCase(),
				registrationExpiration: expirationDate
			});
		}.bind(this)).then(function(insertedUser) {
			//TODO: Send email verification
			return insertedUser;
		});
	},
	
	finalRegister: function(registrationKey, password, dataSession) {
		var self = this,
			verificationQuery = new Query(self.userType, {registrationKey: registrationKey}, {limit: 1, disableSecurity: true}),
			currentDate = new Date(),
			userId = null;
		
		return dataSession.readGeneric(verificationQuery).then(function(found) {
			if(found.length < 1) throw new Level2Exception('BAD_KEY', 'Invalid registration key');
			var registeringUser = found[0];
			
			// Verify the registration has not expired
			if(registeringUser.registrationExpiration < currentDate) throw new Level2Exception('BAD_KEY', 'Invalid registration key');
			userId = found[0].id;
			
			// Verify password complexity
			if(typeof password !== 'string') throw new Level2Exception('BAD_ARG', 'The password must be a string');
			if(password.length < 7) throw new Level2Exception('BAD_ARG', 'The password must be at least 7 characters long');
			
			// Encrypt the password
			return q.nfcall(bcrypt.genSalt, 10).then(function(salt) {
				return q.nfcall(bcrypt.hash, password, salt);
			});
		}).then(function(encryptedPassword) {
			// Finish registering the user
			return dataSession.update(self.userType, userId, {
				registrationKey: null,
				password: encryptedPassword
			});
		});
	},
	
	getPublicUser: function() { return { id: '', groups: [] }; },
	
	_createSession: function(userId, dataSession) {
		// Creates or re-uses a session and returns a promise for the session record
		var self = this;
		
		return q.fcall(function() { // Verify the arguments
			if(!userId) throw new Level2Exception('BAD_ARG', 'userId passed to _createSession can not be blank');
			
		}).then(function() { // Find an existing session
			var existingQuery = new Query(self.sessionType, {user: userId}, {limit: 1, disableSecurity: true});
			return dataSession.readGeneric(existingQuery);
			
		}).then(function(sessions) {
			var newExpiration = self._getCurrentExpiration();
			
			// Use an existing session
			if(sessions.length > 0 && sessions[0].invalidAfter > new Date()) {
				return dataSession.update(self.sessionType, sessions[0].id, {invalidAfter: newExpiration});
			}
			
			// Create a new session
			return q.nfcall(crypto.randomBytes, 20).then(function(rawSessionKey) {
				return dataSession.create(self.sessionType, {
					user: userId,
					invalidAfter: newExpiration,
					key: rawSessionKey.toString('hex').toUpperCase(),
					loggedOut: false
				});
			});
		});
	},
	
	_getCurrentExpiration: function() {
		var newExpiration = new Date();
		newExpiration.setSeconds(newExpiration.getSeconds() + this.sessionPeriod);
		
		return newExpiration;
	}
});