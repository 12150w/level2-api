/* 
 * Endpoint
 * 
 */
var Class = require('./base/class'),
	Level2Exception = require('./exceptions/level2-exception'),
	q = require('q');

module.exports = Class.extend({
	init: function(httpMethod, requestHandler) {
		this._method = null;
		
		// Handle requestHandler without method
		if(requestHandler === undefined) {
			requestHandler = httpMethod;
		} else {
			this._method = httpMethod.toLowerCase();
		}
		this._handler = requestHandler;
		
		if(typeof requestHandler !== 'function') {
			throw new Level2Exception('BAD_ARG', 'The requestHandler must be a function');
		}
		
		// Prepare service parameters (not set until endpoint service is registered)
		this.api = null;
		this.fullPath = null;
		this.service = null;
	},
	
	processRequest: function(req, res, next) {
		return this._handler(req, res, next);
	},
	
	preProcess: function(req, res, next) {
		return q.fcall(function() {});
	}
});