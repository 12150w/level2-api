/* 
 * Class
 * 
 * NOTE: Based on the script by John Resig (http://ejohn.org/)
 */

var initializing = false,
	fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;

// The base Class implementation (does nothing)
var Class = function(){};

// Create a new Class that inherits from this class
Class.extend = function() {
	var _super = this.prototype;
	
	// Instantiate a base class (but only create the instance,
	// don't run the init constructor)
	initializing = true;
	var prototype = new this();
	initializing = false;
	
	// Copy the properties over onto the new prototype
	var prop;
	for(var _i=0; _i<arguments.length; _i++) {
		prop = _i === arguments.length - 1 ? arguments[_i] : arguments[_i].prototype;
		for(var name in prop) {
			
			// Check if we're overwriting an existing function
			if(
				   typeof prop[name] == "function"
				&& typeof _super[name] == "function"
				&& fnTest.test(prop[name])
			) {
				prototype[name] = (function(name, fn){
					return function() {
						var tmp = this._super;
						
						// Add a new ._super() method that is the same method
						// but on the super-class
						this._super = _super[name];
						
						// The method only need to be bound temporarily, so we
						// remove it when we're done executing
						var ret = fn.apply(this, arguments);				
						this._super = tmp;
						
						return ret;
					};
				})(name, prop[name]);
			}
			
			else {
				prototype[name] = prop[name];
			}
		}
	}
	
	// The dummy class constructor
	function SubClass() {
		// All construction is actually done in the init method
		if ( !initializing && this.init )
			this.init.apply(this, arguments);
	}
	
	// Populate our constructed prototype object
	SubClass.prototype = prototype;
	
	// Enforce the constructor to be what we expect
	SubClass.prototype.constructor = Class;
	
	// And make this class extendable
	SubClass.extend = arguments.callee;
	
	return SubClass;
};

module.exports = Class;
