/* 
 * Query
 * 
 */
var Class = require('./base/class');

module.exports = Class.extend({
	_typeName: null,
	_filter: null,
	_options: null,
	
	init: function(typeName, filter, options) {
		this._typeName = typeName;
		this._filter = filter;
		this._options = options || {};
	},
	
	getTypeName: function() {
		return this._typeName;
	},
	
	getFilter: function() {
		return this._filter;
	},
	
	getOptions: function() {
		return this._options;
	}
});