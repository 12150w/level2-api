/* 
 * API
 * 
 */
var Class = require('./base/class'),
	Level2Exception = require('./exceptions/level2-exception'),
	DataSource = require('./data-source'),
	AuthProvider = require('./auth-provider'),
	DataSession = require('./data-session'),
	q = require('q'),
	bodyParser = require('body-parser'),
	
	DataService = require('./services/data'),
	AuthService = require('./services/auth');

module.exports = Class.extend({
	
	init: function(apiName) {
		this.name = apiName;
		this.sessionKey = 'x-level2-sid';
		this.store = null;
		this.authProvider = null;
		
		this._app = null;
		this.basePath = null;
		
		this._preRegister = [];
	},
	
	run: function(app, basePath) {
		var self = this;
		self._app = app;
		self.basePath = basePath;
		
		// Prepare the path
		if(self.basePath == null) {
			self.basePath = '/';
		}
		
		// Read the body
		self._app.use(self.basePath, bodyParser.json());
		
		// Add response.sendError and X-Powered-By
		self._app.use(self.basePath, function(req, res, next) {
			res._hasSentError = false;
			res.sendError = self._sendError;
			next();
		});
		self._app.use(self.basePath, function(req, res, next) {
			res.set('X-Powered-By', 'level2');
			next();
		});
		
		// Add the user's session
		self._app.use(self.basePath, function(req, res, next) {
			return q.fcall(function() { // Check if there is a data store
				if(self.store === null) throw new Level2Exception('INCOMPLETE', 'No DataSource has been set for this API');
				
			}).then(function() { // Find the user
				if(self.authProvider === null) return {id: null, groups: [], isPublic: true};
				if(!!req.get(self.sessionKey)) {
					return self.authProvider.validate(
						req.get(self.sessionKey),
						self.store.createSession({id: null, groups: []})
					);
				}
				
				var publicUser = self.authProvider.getPublicUser();
				publicUser.isPublic = true;
				return publicUser;
				
			}).then(function(user) { // Set the session
				req.session = self.store.createSession(user);
				if(user.isPublic === true) req.session.isPublic = true;
				next();
				
			}).fail(function(err) { // Handle failure to find user
				res.sendError('Failed to load session: ' + err, err.extra || 'INTERNAL_ERR', err.statusCode || 500);
			});
		});
		
		// Included services
		self.registerService('/data', new DataService());
		self.registerService('/auth', new AuthService());
		
		// Pre registered services
		for(var _i=0; _i<self._preRegister.length; _i++) {
			self._injectService(self._preRegister[_i].base, self._preRegister[_i].service);
		}
		
		// Root Handler
		self._app.get(self.basePath, self._rootHandler.bind(self));
		
		// Custom Error Handling
		self._app.use(self.basePath, self._notFoundHandler.bind(self));
		
		var route;
		return this;
	},
	
	setSource: function(dataSource) {
		if(dataSource instanceof DataSource !== true) throw new Level2Exception('BAD_ARG', 'dataSource must be a DataSource instance');
		this.store = dataSource;
		
		return this;
	},
	
	setAuthProvider: function(provider) {
		if(provider instanceof AuthProvider !== true) throw new Level2Exception('BAD_ARG', 'provider must be an AuthProvider instance');
		this.authProvider = provider;
		
		return this;
	},
	
	registerService: function(basePath, service) {
		if(this._app === null) {
			return this._preRegister.push({
				service: service, base: basePath
			});
		}
		this._injectService(basePath, service);
		
		return this;
	},
	
	_injectService: function(base, service) {
		var useAPIBase = this.basePath;
		if(useAPIBase === '/') useAPIBase = '';
		
		service._register(useAPIBase + base, this, this._app);
	},
	
	_notFoundHandler: function(req, res) {
		res.sendError(
			'invalid endpoint or method ' + req.method + ' ' + req.path,
			'NOT_FOUND',
			404
		);
	},
	
	_rootHandler: function(req, res, next) {
		if(req.path !== this.basePath) return next();
		res.status(200).json({
			name: this.name
		});
	},
	
	_sendError: function(msg, code, httpStatus, extraAttrs) {
		if(typeof httpStatus !== 'number') httpStatus = 500;
		
		// Skip if error has already been sent
		if(this._hasSentError === true) return;
		this._hasSentError = true;
		
		var baseInfo = {
			code: code,
			error: msg
		};
		
		if(typeof extraAttrs === 'object') {
			for(var key in extraAttrs) {
				if(key === 'code' || key === 'error') continue;
				baseInfo[key] = extraAttrs[key];
			}
		}
		
		this.status(httpStatus).json(baseInfo);
		
		return this;
	},
	
	_cookieEscape: function(raw) {
		if(raw == null) return '';
		return raw.replace(/ /g, '-').replace(/[;~!@#\$%\^\&\*\(\)\[\]]/g, '');
	}
});