/* 
 * DataSource
 * 
 */
var Class = require('./base/class'),
	DataAdapter = require('./data-adapter'),
	Level2Exception = require('./exceptions/level2-exception'),
	DataSession = require('./data-session');

module.exports = Class.extend({
	init: function() {
		this._adapters = [];
		
		// Copy adapters to this instance
		for(var _i=0; _i<arguments.length; _i++) {
			if(arguments[_i] instanceof DataAdapter === false) {
				throw new Level2Exception('BAD_ARG', 'DataSource init only accepts DataAdapter instances');
			}
			this._adapters.push(arguments[_i]);
		}
	},
	
	createSession: function(user, adapterKey) {
		// Get the adapter to use
		if(this._adapters.length < 1) throw new Level2Exception('NOT_FOUND', 'This DataSource has no adapters');
		var adapter = this._adapters[0];
		if(typeof adapterKey === 'number') {
			if(adapterKey >= this._adapters.length) throw new Level2Exception('NOT_FOUND', 'No adapter for key ' + adapterKey);
			adapter = this._adapters[adapterKey];
		}
		
		return new DataSession(user, adapter);
	}
});