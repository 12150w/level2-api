/* 
 * Starts an Empty API with a memory data adapter
 * 
 */
var level2 = require('..'),
	express = require('express'),
	port = 1215,

	DemoAPI = new level2.API('Blank Demo API'),
	demoApp = express();

// Set up the database
DemoAPI.setSource(new level2.DataSource(new level2.DataAdapter()));

// Set up the authentication
DemoAPI.setAuthProvider(new level2.AuthProvider());

DemoAPI.run(demoApp);
demoApp.listen(port, function() {
	console.log('Started API on port ' + port);
});