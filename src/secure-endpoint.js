/* 
 * SecureEndpoint
 * 
 */
var Endpoint = require('./endpoint'),
	q = require('q');

module.exports = Endpoint.extend({
	preProcess: function(req, res, next) {
		var self = this;
		
		return q.fcall(function() {
			if(req.session.isPublic === true) {
				res.sendError('This endpoint may only be used by authorized users', 'UNAUTHORIZED', 401);
				throw new Error();
			}
		});
	}
});