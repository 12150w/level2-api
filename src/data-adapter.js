/* 
 * DataAdapter
 * 
 */
var Class = require('./base/class'),
	Level2Exception = require('./exceptions/level2-exception'),
	SharingOption = require('./sharing-option'),
	q = require('q');

module.exports = Class.extend({
	_cache: null,
	_counter: 0,
	_securityField: 'l2-security-options',
	
	init: function(name) {
		this._name = name;
		this._cache = {};
	},
	
	commit: function(typeName, data) {
		return q.fcall(function() {
			if(!typeName) throw new Level2Exception('BAD_ARG', 'typeName can not be null');
			if(typeof(data) !== 'object' || !data) data = {};
			
			if(!this._cache[typeName]) this._cache[typeName] = {};
			data.id = ++this._counter;
			this._cache[typeName][data.id] = data;
			
			return data;
		}.bind(this));
	},
	
	extract: function(typeName, query) {
		return q.fcall(function() {
			if(query === null || query === NaN) return [];
			if(!typeName) throw new Level2Exception('BAD_ARG', 'typeName can not be null');
			if(!this._cache[typeName]) this._cache[typeName] = {};
			
			// Check for id query
			if(typeof(query) !== 'object') query = {id: query};
			
			var record, id, attrName, matching, compareAttr, sharingRule, _i, hasSharingAccess,
				results = [];
			for(id in this._cache[typeName]) {
				record = this._cache[typeName][id];
				matching = true;
				
				// Filter this record
				for(attrName in query) {
					if(
						matching === false
						|| (!record.hasOwnProperty(attrName) && String(attrName).substring(0, 1) !== '_')
					) continue;
					
					// Less than operators
					if(attrName === '_greaterThan') {
						for(compareAttr in query[attrName]) {
							// Note: If record is greater it is not less than or equal
							if(record[compareAttr] <= query[attrName][compareAttr]) {
								matching = false;
								break;
							}
						}
						continue;
					}
					
					// Security check
					if(attrName === '_user') {
						if(!record[this._securityField]) continue;
						hasSharingAccess = false;
						
						// Check each rule
						for(_i=0; _i<record[this._securityField].length; _i++) {
							sharingRule = record[this._securityField][_i];
							
							// Verify write level
							if(sharingRule.accessLevel < 1) continue;
							
							if(sharingRule.type == SharingOption.type.USER) {
								if(query[attrName].id === sharingRule.target) {
									hasSharingAccess = true;
									break;
								}
							} else if(sharingRule.type == SharingOption.type.GROUP) {
								if(query[attrName].groups.indexOf(sharingRule.target) !== -1) {
									hasSharingAccess = true;
									break;
								}
							} else if(sharingRule.type === SharingOption.type.EVERYONE) {
								hasSharingAccess = true;
								break;
							}
						}
						
						if(hasSharingAccess !== true) {
							matching = false;
						}
						continue;
					}
					
					// Equals operators
					if(record[attrName] !== query[attrName]) {
						matching = false;
						continue;
					}
				}
				
				if(matching === true) results.push(record);
			}
			
			return results;
		}.bind(this));
	},
	
	update: function(typeName, data) {
		return q.fcall(function() {
			if(!this._cache[typeName]) throw new Level2Exception('BAD_ARG', 'no type found with name ' + typeName);
			
			// Find the existing record
			var existing = this._cache[typeName][data.id];
			if(!existing) throw new Level2Exception('NOT_FOUND', 'no record found with id ' + data.id);
			
			// Remove sharing attributes
			delete data[this._securityField];
			
			// Copy in the new data
			for(var newKey in data) {
				if(newKey === 'id') continue;
				existing[newKey] = data[newKey];
			}
			this._cache[typeName][data.id] = existing;
			
			return existing;
		}.bind(this));
	},
	
	destroy: function(typeName, id) {
		return q.fcall(function() {
			if(!this._cache[typeName]) throw new Level2Exception('No type found with name ' + typeName);
			if(!this._cache[typeName][id]) throw new Level2Exception('No record found with id ' + id);
			
			// Destroy the record
			delete this._cache[typeName][id];
		}.bind(this));
	},
	
	convertQuery: function(query) {
		var baseQuery = query.getFilter() || {};
		if(!!query.getOptions().greaterThan) {
			baseQuery._greaterThan = query.getOptions().greaterThan;
		}
		
		return baseQuery;
	},
	
	getTypes: function() {
		var self = this;
		return q.fcall(function() {
			return Object.keys(self._cache);
		});
	},
	
	addSharingBefore: function(typeName, options, data) {
		return q.fcall(function() {
			data[this._securityField] = options;
			return data;
		}.bind(this));
	},
	
	addSharingAfter: function(typeName, options, data) {
		return q.fcall(function() {});
	},
	
	addSharingToQuery: function(typeName, query, user) {
		return q.fcall(function() {
			query._user = user;
			return query;
		});
	},
	
	getSecurityLevel: function(record, user) {
		var currentLevel = 0;
		
		var opt;
		for(var _i=0; _i<record[this._securityField].length; _i++) {
			opt = record[this._securityField][_i];
			switch(opt.type) {
				case SharingOption.type.USER:
					if(opt.target === user.id && opt.accessLevel > currentLevel) {
						currentLevel = opt.accessLevel;
					}
					break;
				case SharingOption.type.GROUP:
					if(user.groups.indexOf(opt.target) > -1 && opt.accessLevel > currentLevel) {
						currentLevel = opt.accessLevel;
					}
					break;
				case SharingOption.type.EVERYONE:
					if(opt.accessLevel > currentLevel) {
						currentLevel = opt.accessLevel;
					}
					break;
			}
		}
		
		return currentLevel;
	}
});