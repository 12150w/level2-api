/* 
 * Tests the Endpoint class
 * 
 */
var level2 = require('..'),
	dev = require('../dev-modules'),
	assert = require('assert'),
	express = require('express'),
	request = require('request');

describe('Endpoint', function() {
	var dummyReq = {}, dummyRes = {}, dummyNext = function(){};
	
	describe('#processRequest', function() {
		it('Runs the request handler', function() {
			var handlerHasRun = false;
			var testEndpoint = new level2.Endpoint(function() {
				handlerHasRun = true;
			});
			
			testEndpoint.processRequest({}, {}, function(){});
			assert.ok(handlerHasRun === true, 'The request handler was not run');
		});
		
		it('Passes the request, response, and next to the handler', function(done) {
			var testEndpoint = new level2.Endpoint(function(req, res, next) {
				assert.ok(req === dummyReq, 'The passed in req is not correct');
				assert.ok(res === dummyRes, 'The passed in res is not correct');
				assert.ok(next === dummyNext, 'The passed in next is not correct');
				
				done();
			});
			
			testEndpoint.processRequest(dummyReq, dummyRes, dummyNext);
		});
		
		it('Sets the api, fullPath, and service properties by the time the requests are processed', function(done) {
			var testAPI = new dev.TestAPI('Test API'),
				hasPath = false,
				hasAPI = false,
				hasService = false;
				TestService = level2.Service.extend({
					endpoints: {
						'properties': function(req, res) {
							if(!!this.fullPath) hasPath = true;
							if(this.api instanceof level2.API) hasAPI = true;
							if(this.service instanceof level2.Service) hasService = true;
							
							res.json({});
						}
					}
				});
			
			testAPI.registerService('/test', new TestService());
			
			testAPI.start().then(function() {
				return testAPI.request({uri: '/test/properties', method: 'GET'});
			}).then(function() {
				assert.ok(hasPath === true, 'The fullPath property was not set during request processing');
				assert.ok(hasAPI === true, 'The api property was not set during request processing');
				assert.ok(hasService === true, 'The service property was not set during request processing');
				
				return testAPI.stop();
			}).then(function() {
				done();
			}).done();
		});
	});
	
	describe('#preProcess', function() {
		var testEndpoint;
		beforeEach('Create a test endpoint', function() {
			testEndpoint = new level2.Endpoint(function() {});
		});
		
		it('Returns a promise that always resolves', function(done) {
			testEndpoint.preProcess(dummyReq, dummyRes, dummyNext).then(function() {
				done();
			}, function(err) {
				done(err);
			}).done();
		});
	});
});