/* 
 * Tests the Service class
 * 
 */
var level2 = require('..'),
	assert = require('assert');

describe('Service', function() {
	var TestService;
	
	before('Define a testing service', function() {
		TestService = level2.Service.extend({
		});
	});
	
	describe('#init', function() {
		it('Creates a new instance', function() {
			var service = new TestService();
			assert.ok(service instanceof level2.Service);
		});
	});
});