/* 
 * Tests the Query object
 * 
 */
var level2 = require('..'),
	assert = require('assert');

describe('Query', function() {
	var testQuery;
	beforeEach('Create a query', function() {
		testQuery = new level2.Query('test', {a: 1});
	});
	
	describe('#getTypeName', function() {
		it('Returns the query type name', function() {
			assert.equal(testQuery.getTypeName(), 'test', 'The returned name matches init name');
		});
	});
	
	describe('#getFilter', function() {
		it('Returns the raw query filter', function() {
			var filter = testQuery.getFilter();
			assert.equal(filter.a, 1, 'The returned filter matches init filter');
		});
	});
	
	describe('#getOptions', function() {
		it('Returns the query options', function() {
			var opts = testQuery.getOptions();
			assert.equal(JSON.stringify(opts), '{}', 'The returned options match');
		});
	});
});