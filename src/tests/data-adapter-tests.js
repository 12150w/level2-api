/* 
 * Tests the DataSource and DataAdapter classes
 * 
 */
var level2 = require('..'),
	assert = require('assert');

describe('DataAdapter', function() {
	var testAdapter;
		
	beforeEach('Create instance', function() {
		testAdapter = new level2.DataAdapter();
	});
	
	describe('#commit', function() {
		it('Adds the item to the data store', function(done) {
			testAdapter.commit('test', {a: 1, b: 2}).then(function(inserted) {
				assert.ok(inserted.id != null, 'All inserted records must have an id generated');
				assert.ok(testAdapter._cache.test[inserted.id] != null, 'The record was not found in the adapter\'s source');
				done();
			}).done();
		});
	});
	
	describe('#extract', function() {
		var inserted;
		beforeEach('Insert something', function(done) {
			testAdapter.commit('test', {a: 1, b: 2}).then(function(newRecord) {
				inserted = newRecord;
				done();
			}).done();
		});
		
		it('Allows filtering by id', function(done) {
			testAdapter.extract('test', inserted.id).then(function(res) {
				assert.ok(res != null, 'One record should be found');
				assert.equal(res[0].id, 1, 'The queried record id should match the inserted one');
				
				done();
			}).done();
		});
		
		it('Allows filtering by query', function(done) {
			testAdapter.extract('test', {a: inserted.a}).then(function(res) {
				assert.ok(res != null, 'One record should have been found');
				assert.equal(res[0].id, 1, 'The queried record id should match the inserted one');
				
				done();
			}).done();
		});
		
		it('Allows filtering for less than using _greaterThan filter key', function(done) {
			testAdapter.extract('test', {_greaterThan: {a: 0}}).then(function(found) {
				assert.ok(found.length === 1, 'The record was not queried');
				return testAdapter.extract('test', {_greaterThan: {a: 1}});
			}).then(function(found) {
				assert.ok(found.length === 0, 'The record should not have been queried');
			}).then(function() {
				done();
			}).done();
		});
		
		it('Returns an empty Array when no records match the query', function(done) {
			testAdapter.extract('test', {a: inserted.a + 'aha'}).then(function(res) {
				assert.equal(JSON.stringify(res), '[]', 'An empty Array should be resolved when no query matches');
				
				done();
			}).done();
		});
	});
	
	describe('#update', function() {
		var inserted;
		beforeEach('Insert something', function(done) {
			testAdapter.commit('test', {a: 1, b: 2}).then(function(newRecord) {
				inserted = newRecord;
				done();
			}).done();
		});
		
		it('Allows updating by id', function(done) {
			inserted.a = 'UPDATED';
			testAdapter.update('test', inserted).then(function(updated) {
				assert.equal(updated.a, 'UPDATED', 'The field should have been updated');
				done();
			}).done();
		});
	});
	
	describe('#destroy', function() {
		var inserted;
		beforeEach('Insert something', function(done) {
			testAdapter.commit('test', {a: 1, b: 2}).then(function(newRecord) {
				inserted = newRecord;
				done();
			}).done();
		});
		
		it('Allows deleting by id', function(done) {
			testAdapter.destroy('test', inserted.id).then(function() {
				return testAdapter.extract('test', inserted.id);
			}).then(function(found) {
				assert.ok(found.length === 0, 'The record is still in the data store');
				done();
			}).done();
		})
	});
	
	describe('#convertQuery', function() {
		it('Returns the query filter', function() {
			var testQuery = new level2.Query('test', {a: 1}),
				converted = testAdapter.convertQuery(testQuery);
			
			assert.ok(converted.a === 1);
		})
	});
	
	describe('#getTypes', function() {
		it('Returns existing types', function(done) {
			testAdapter.commit('something', {}).then(function() {
				return testAdapter.getTypes();
			}).then(function(types) {
				assert.ok(types.indexOf('something') !== -1, 'Did not resolve with all existing types');
				done();
			}).done();
		});
	});
});