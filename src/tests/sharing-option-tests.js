/* 
 * Tests the SharingOption class
 * 
 */
var level2 = require('..'),
	SharingOption = level2.SharingOption,
	assert = require('assert');

describe('SharingOption', function() {
	describe('#init', function() {
		it('Sets the passed in attributes on the instance', function() {
			var testOpt = new SharingOption(
				SharingOption.type.GROUP,
				SharingOption.level.DELETE,
				'test-group'
			);
			
			assert.ok(testOpt.type === SharingOption.type.GROUP, 'The group attribute should be set');
			assert.ok(testOpt.accessLevel === SharingOption.level.DELETE, 'The accessLevel attribute should be set');
			assert.ok(testOpt.target === 'test-group', 'The target attribute should be set');
		});
	});
});