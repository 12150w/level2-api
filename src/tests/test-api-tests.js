/* 
 * Tests the TestAPI class
 * 
 */
var dev = require('../dev-modules'),
	level2 = require('..'),
	q = require('q'),
	net = require('net'),
	assert = require('assert');

describe('TestAPI', function() {
	var testApi;
	
	beforeEach('Create a TestAPI instance', function() {
		testApi = new dev.TestAPI('Testing API');
	});
	
	describe('#start', function() {
		it('Starts a server using a UNIX socket or named pipe', function(done) {
			testApi.start().then(function() {
				assert.ok(!!testApi.socketPath, 'The socketPath was not set after start was called');
				
				var socketDefer = q.defer();
				var socket = net.connect(testApi.socketPath, function(err) {
					if(err) socketDefer.reject(err);
					else socketDefer.resolve(socket);
				});
				
				return socketDefer.promise;
			}).then(function(socket) {
				var closeDefer = q.defer();
				socket.on('close', function(hadError) {
					if(hadError) closeDefer.reject(new Error('The socket was closed due to an error'));
					else closeDefer.resolve();
				});
				socket.end();
				
				return closeDefer.promise;
			}).then(function() {
				return testApi.stop();
			}).then(function() {
				done();
			}).done();
		});
		it('Allows changing the server base url', function(done) {
			testApi.start('/unit-test').then(function() {
				return testApi.apiRequest({uri: '/unit-test', method: 'GET'});
			}).then(function(res) {
				return testApi.stop();
			}).then(function() {
				done();
			}).done();
		});
	});
	
	describe('#stop', function() {
		it('Stops the server', function(done) {
			testApi.start().then(function() {
				return testApi.stop();
			}).then(function() {
				done();
			}).done();
		});
	});
	
	describe('#request', function() {
		beforeEach('Start the test API', function(done) {
			testApi.start().then(function() {
				done();
			}).done();
		});
		
		it('Sends a request to the test API', function(done) {
			testApi.request({uri: '/', method: 'GET'}).then(function(response) {
				assert.ok(!!response.message, 'Resolved without the http message');
				assert.ok(!!response.body, 'Resolved without the request response');
				
				done();
			}).done();
		});
		
		afterEach('Stop the test API', function(done) {
			testApi.stop().then(function() {
				done();
			}).done();
		});
	});
	
	describe('#apiRequest', function() {
		beforeEach('Start the test API', function(done) {
			testApi.start().then(function() {
				done();
			}).done();
		});
		
		it('Fails with the error when HTTP status does not indicate success', function(done) {
			testApi.apiRequest({uri: '/not-an-existing-endpoint', method: 'GET'}).then(function() {
				done(new Error('Promise should not have resolved'));
			}).fail(function(err) {
				assert.ok(err instanceof level2.APIException, 'Failed requests should resolve with an APIException');
				done();
			}).done();
		});
		
		afterEach('Stop the test API', function(done) {
			testApi.stop().then(function() {
				done();
			}).done();
		});
	});
	
	describe('#getSecureSession', function() {
		it('Resolves to a DataSession instance', function() {
			testApi.getSecureSession().then(function(session) {
				assert.ok(session instanceof level2.DataSession);
				
				done();
			}).done();
		});
	});
});