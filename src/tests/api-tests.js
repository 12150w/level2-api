/* 
 * Tests the API class
 * 
 */
var level2 = require('..'),
	assert = require('assert'),
	dev = require('../dev-modules');

describe('API', function() {
	describe('#init', function() {
		it('Sets the name', function() {
			var initTestAPI = new level2.API('Test Name');
			assert.equal(initTestAPI.name, 'Test Name', 'The API instance name attribute should match init args');
		});
	});
});