/* 
 * Tests the AuthProvider class
 * 
 */
var level2 = require('..'),
	assert = require('assert'),
	bcrypt = require('bcrypt'),
	q = require('q'),
	
	testRegistration = {
		username: 'some_username',
		email: 'unittest@12150w.com'
	};

describe('AuthProvider', function() {
	var testSource, testSession, testProvider,
		testPassword = '#Th1s_i3a&st@@t^pas**ord~!';
	
	beforeEach('Create a blank data source', function() {
		testSource = new level2.DataSource(new level2.DataAdapter());
		testSession = testSource.createSession({id: 1, groups: []});
	});
	beforeEach('Create a new auth provider', function() {
		testProvider = new level2.AuthProvider();
	});
	
	describe('#initialRegister', function() {
		it('Returns the inserted user with registration key and expiration', function(done) {
			var registeredUser;
			
			testProvider.initialRegister(testRegistration, testSession).then(function(user) {
				registeredUser = user;
				assert.ok(!!user.registrationKey, 'Registration key was not set');
				assert.ok(user.registrationExpiration instanceof Date, 'Registration expiration was not set or not a date');
				
				return testSession.readGeneric(new level2.Query(testProvider.userType, {username: testRegistration.username}, {limit: 1, disableSecurity: true}));
			}).then(function(users) {
				assert.ok(users.length > 0, 'No user was inserted');
				assert.ok(users[0].registrationKey === registeredUser.registrationKey, 'The registration key does not match');
				
				done();
			}).done();
		});
	});
	
	describe('#finalRegister', function() {
		var initialUser;
		
		beforeEach('Do initial registration', function(done) {
			testProvider.initialRegister(testRegistration, testSession).then(function(user) {
				initialUser = user;
				done();
			}).done();
		});
		
		it('Registers a valid registration by setting the key to null', function(done) {
			testProvider.finalRegister(initialUser.registrationKey, testPassword, testSession).then(function() {
				return testSession.readGeneric(new level2.Query(
					testProvider.userType,
					{username: testRegistration.username},
					{limit: 1, disableSecurity: true}
				));
			}).then(function(found) {
				assert.ok(found[0].registrationKey === null, 'The user was not registered');
				
				done();
			}).done();
		});
		
		it('Prevents registration with an invalid registration key', function(done) {
			testProvider.finalRegister('SOME_INVALID_KEY', testPassword, testSession).then(function() {
				done(new Error('Promise should not have resolved'));
			}, function(err) {
				done();
			}).done();
		});
		
		it('Prevents registration with an overdue expiration date', function(done) {
			var pastExpirationDate = new Date();
			pastExpirationDate.setDate(pastExpirationDate.getDate() - 7);
			
			testSession.update(testProvider.userType, initialUser.id, {
				registrationExpiration: pastExpirationDate
			}).then(function() {
				return testProvider.finalRegister(initialUser.registrationKey, testPassword, testSession);
			}).then(function() {
				done(new Error('Promise should not have resolved'));
			}).fail(function(err) {
				done();
			}).done();
		});
		
		it('Encrypts the password using bcrypt', function(done) {
			testProvider.finalRegister(initialUser.registrationKey, testPassword, testSession).then(function() {
				return testSession.readGeneric(new level2.Query(
					testProvider.userType,
					{username: initialUser.username},
					{limit: 1, disableSecurity: true}
				));
			}).then(function(users) {
				assert.ok(!!users[0].password, 'The password was not set');
				
				return q.nfcall(bcrypt.compare, testPassword, users[0].password);
			}).then(function(passwordsMatch) {
				assert.ok(passwordsMatch === true, 'The encrypted password does not match');
				
				done();
			}).done();
		});
	});
	
	describe('#login', function() {
		var initialUser, validLoginIdentity;
		
		beforeEach('Register a user', function(done) {
			testProvider.initialRegister(testRegistration, testSession).then(function(user) {
				initialUser = user;
				validLoginIdentity = {username: initialUser.username, password: testPassword};
				
				return testProvider.finalRegister(initialUser.registrationKey, testPassword, testSession);
			}).then(function() {
				done();
			}).done();
		});
		
		it('Succeeds with a valid username and password', function(done) {
			testProvider.login({username: initialUser.username, password: testPassword}, testSession).then(function(sid) {
				assert.ok(!!sid, 'The resolved session id is blank');
				
				done();
			}).done();
		});
		
		it('Fails with an invalid username', function(done) {
			testProvider.login({username: initialUser.username + '_not', password: testPassword}, testSession).then(function() {
				done(new Error('The promise should not have resolved'));
			}, function() {
				done();
			});
		});
		
		it('Fails with an invalid password', function(done) {
			testProvider.login({username: initialUser.username, password: testPassword + '_not'}, testSession).then(function() {
				done(new Error('The promise should not have resolved'));
			}, function() {
				done();
			});
		});
		
		it('Re-Uses sessions that are not expired for the user logging in', function(done) {
			var firstSID;
			
			testProvider.login(validLoginIdentity, testSession).then(function(sid) {
				firstSID = sid;
				return testProvider.login(validLoginIdentity, testSession);
			}).then(function(secondSID) {
				assert.ok(firstSID === secondSID, 'The SID was not reused because they do not match');
				
				done();
			}).done();
		});
		
		it('Creates a new session when login is called after previous session expires', function(done) {
			var firstSID;
			
			testProvider.login(validLoginIdentity, testSession).then(function(sid) {
				firstSID = sid;
				
				var findSessionQuery = new level2.Query(testProvider.sessionType, {key: sid}, {limit: 1, disableSecurity: true});
				return testSession.readGeneric(findSessionQuery);
			}).then(function(foundSessions) {
				var sessionId = foundSessions[0].id;
				var newInvalidDate = new Date();
				newInvalidDate.setDate(newInvalidDate.getDate() - 1);
				
				testSession.update(testProvider.sessionType, sessionId, {invalidAfter: newInvalidDate});
			}).then(function() {
				return testProvider.login(validLoginIdentity, testSession);
			}).then(function(newSID) {
				assert.ok(firstSID !== newSID, 'The session id did not change after session expired');
				
				done();
			}).done();
		})
	});
	
	describe('#validate', function() {
		var initialUser, validLoginIdentity, validSID;
		
		beforeEach('Register a user', function(done) {
			testProvider.initialRegister(testRegistration, testSession).then(function(user) {
				initialUser = user;
				validLoginIdentity = {username: initialUser.username, password: testPassword};
				
				return testProvider.finalRegister(initialUser.registrationKey, testPassword, testSession);
			}).then(function() {
				done();
			}).done();
		});
		beforeEach('Log the user in', function(done) {
			testProvider.login(validLoginIdentity, testSession).then(function(sid) {
				validSID = sid;
			}).then(function() {
				done();
			}).done();
		});
		
		it('Resolves the session user', function(done) {
			testProvider.validate(validSID, testSession).then(function(sessionUser) {
				assert.ok(sessionUser.id === initialUser.id, 'The session did not resolve to the matching user');
				assert.ok(!sessionUser.password, 'The session user should not include the encrypted password');
			}).then(function() {
				done();
			}).done();
		});
		
		it('Does not validate expired sessions', function(done) {
			var pastDate = new Date();
			pastDate.setHours(pastDate.getHours() - 1);
			var findSessionQuery = new level2.Query(testProvider.sessionType, {key: validSID}, {limit: 1, disableSecurity: true});
			
			testSession.readGeneric(findSessionQuery).then(function(sessions) { // Update the session to be invalid
				var userSession = sessions[0]
				return testSession.update(testProvider.sessionType, userSession.id, {invalidAfter: pastDate});
				
			}).then(function() { // Try to validate the invalid session
				return testProvider.validate(validSID, testSession);
				
			}).then(function() {
				done(new Error('The validation should not have resolved'));
			}, function() {
				done();
			}).done();
		});
		
		it('Does not validate bad session sid', function(done) {
			testProvider.validate(validSID + '_bad', testSession).then(function() {
				done(new Error('Validate should not have resolved'));
			}, function() {
				done();
			}).done();
		});
	});
	
	describe('#logout', function() {
		var initialUser, validLoginIdentity, validSID;
		
		beforeEach('Register a user', function(done) {
			testProvider.initialRegister(testRegistration, testSession).then(function(user) {
				initialUser = user;
				validLoginIdentity = {username: initialUser.username, password: testPassword};
				
				return testProvider.finalRegister(initialUser.registrationKey, testPassword, testSession);
			}).then(function() {
				done();
			}).done();
		});
		beforeEach('Log the user in', function(done) {
			testProvider.login(validLoginIdentity, testSession).then(function(sid) {
				validSID = sid;
			}).then(function() {
				done();
			}).done();
		});
		
		it('Expires a valid session', function(done) {
			testProvider.logout(validSID, testSession).then(function() { // Try to validate the logged out session
				return testProvider.validate(validSID, testSession);
				
			}).then(function() {
				done(new Error('Should not have resolved validate after logout'));
			}, function(err) {
				done();
			}).done();
		});
	});
});