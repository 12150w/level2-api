/* 
 * Tests the DataSource class
 * 
 */
var level2 = require('..'),
	assert = require('assert');

describe('DataSource', function() {
	var testAdapter, testSrc;
	beforeEach('Create test adapter and data source', function() {
		testAdapter = new level2.DataAdapter();
		testSrc = new level2.DataSource(testAdapter);
	});
	
	describe('#init', function() {
		it('Rejects non DataAdapter arguments', function() {
			assert.throws(function() {
				var badSource = new level2.DataSource('a');
			}, level2.Level2Exception);
		});
		it('Accepts DataAdapter arguments', function() {
			var goodSource = new level2.DataSource(testAdapter);
		});
	});
	
	describe('#createSession', function() {
		it('Returns a session instance', function() {
			var session = testSrc.createSession({});
			assert.ok(session instanceof level2.DataSession);
		});
	});
});