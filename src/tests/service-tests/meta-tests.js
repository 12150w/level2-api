/* 
 * Tests the process of starting an express server
 * 
 */
var level2 = require('../..'),
	dev = require('../../dev-modules'),
	express = require('express'),
	net = require('net'),
	request = require('request'),
	assert = require('assert');

describe('Service Meta', function() {
	var testAPI = new dev.TestAPI('Test API'),
		FailingService = level2.Service.extend({
			endpoints: {
				'break': function(req, res) {
					var test = null.aha;
					res.json({});
				}
			}
		});
	
	before('Start the api', function(done) {
		testAPI.registerService('/fail', new FailingService());
		
		testAPI.start().then(function() {
			done();
		}).done();
	});
	after('Stop the test api', function(done) {
		testAPI.stop().then(function() {
			done();
		}).done();
	});
	
	it('Sets the powered by header', function(done) {
		testAPI.request({uri: '/', method: 'GET'}).then(function(res) {
			assert.equal(res.message.headers['x-powered-by'], 'level2', 'The X-Powered-By header should be set to "level2"');
			done();
		}).done();
	});
	
	describe('Errors', function() {
		it('Returns 404 for an invalid route', function(done) {
			testAPI.request({uri: '/some-random/undefined-location', method: 'GET'}).then(function(res) {
				assert.equal(res.message.statusCode, 404, 'The  response status code should be 404');
				assert.equal(res.body.code, 'NOT_FOUND', 'The error code should be NOT_FOUND');
				done();
			}).done();
		});
		
		it('Returns 500 for an endpoint exception', function(done) {
			testAPI.request({uri: '/fail/break', method: 'GET'}).then(function(res) {
				assert.equal(res.message.statusCode, 500, 'The exception should return 500 status');
				assert.equal(res.body.code, 'EXCEPTION', 'The code should be EXCEPTION');
				assert.ok(res.body.trace != null, 'The exception stack should be returned');
				done();
			}).done();
		});
	});
});