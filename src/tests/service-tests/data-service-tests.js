/* 
 * Tests the data service at /data
 * 
 */
var level2 = require('../..'),
	dev = require('../../dev-modules'),
	express = require('express'),
	request = require('request'),
	assert = require('assert');

describe('Data Service', function() {
	var testAPI = new dev.TestAPI('Test API'),
		testSession;
	this.timeout(1000);
	
	before('Start the api', function(done) {
		testAPI.setAuthProvider(new level2.AuthProvider());
		
		testAPI.start().then(function() {
			done();
		}).done();
	});
	before('Set the test session', function(done) {
		testAPI.getSecureSession().then(function(session) {
			testSession = session;
			done();
		}).done();
	});
	after('Stop the test api', function(done) {
		testAPI.stop().then(function() {
			done();
		}).done();
	});
	
	describe('Root Endpoint (/data)', function() {
		it('Returns a service description  (GET)', function(done) {
			testAPI.apiRequest({uri: '/data', method: 'GET'}).then(function(res) {
				assert.equal(res.message.statusCode, 200, 'Status should be OK');
				assert.ok(res.body.endpoints != null, 'endpoints should be listed');
				done();
			}).done();
		});
	});
		
	describe('Describe Endpoint (/data/describe)', function() {
		describe('GET', function() {
			it('Returns a list of valid types', function(done) {
				testSession.create('some-type', {a: 1}).then(function() {
					return testAPI.secureRequest({uri: '/data/describe', method: 'GET'});
				}).then(function(res) {
					assert.ok(res.body.hasOwnProperty('types'), 'No types were found in response');
					assert.ok(res.body.types.indexOf('some-type') !== -1, 'The types are not correct');
				}).then(function() {
					done();
				}).done();
			});
			
			it('Excludes user and session types', function(done) {
				testAPI.authProvider.initialRegister(
					{username: 'test', email: 'test@test.com'},
					testSession
				).then(function() {
					return testAPI.secureRequest({uri: '/data/describe', method: 'GET'});
				}).then(function(res) {
					assert.ok(res.body.types.indexOf(testAPI.authProvider.userType) === -1, 'The user type was in the description');
				}).then(function() {
					done();
				}).done();
			});
		});
	});
	
	describe('Records Endpoint (/data/records/<TYPE NAME>)', function() {
		describe('POST', function() {
			it('Creates a new record of the given type', function(done) {
				testAPI.secureRequest({
					uri: '/data/records/sample',
					method: 'POST',
					body: {sampleField: 'sample value'}
				}).then(function(res) {
					assert.ok(!!res.body.id, 'The record was not returned after insert');
					
					var findQuery = new level2.Query(
						'sample', {id: res.body.id}, {limit: 1, disableSecurity: true}
					);
					return testSession.readGeneric(findQuery);
				}).then(function(found) {
					assert.ok(found.length === 1, 'The record was not inserted');
					assert.ok(found[0].sampleField === 'sample value', 'The record fields were not saved');
				}).then(function() {
					done();
				}).done();
			});
			
			it('Does not allow access to auth provider types', function(done) {
				testAPI.secureRequest({
					uri: '/data/records/' + testAPI.authProvider.userType,
					method: 'POST',
					body: {}
				}).then(function() {
					done(new Error('Request should not have succeeded'));
				}).fail(function(err) {
					assert.ok(err.statusCode === 400, 'The error code should be 400');
					
					done();
				}).done();
			});
		});
		describe('GET', function() {
			before('Add a record to get', function(done) {
				testSession.create('get-test-type', {key: 'data-get'}).then(function() {
					done();
				}).done();
			});
			
			it('Returns the records of the given type', function(done) {
				testAPI.secureRequest({uri: '/data/records/get-test-type', method: 'GET'}).then(function(res) {
					assert.ok(res.body.records.length > 0, 'No records were returned');
					assert.ok(res.body.records[0].key === 'data-get', 'Unexpected data was returned');
					
				}).then(function() {
					done();
				}).done();
			});
		});
	});
});