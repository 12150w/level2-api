/* 
 * Tests the authentication service at /auth
 * 
 */
var level2 = require('../..'),
	dev = require('../../dev-modules'),
	assert = require('assert');

describe('Authentication Service', function() {
	var testAPI = new dev.TestAPI('Test API');
	
	before('Start the api', function(done) {
		testAPI.setAuthProvider(new level2.AuthProvider());
		
		testAPI.start().then(function() {
			done();
		}).done();
	});
	after('Stop the test api', function(done) {
		testAPI.stop().then(function() {
			done();
		}).done();
	});
	
	describe('Initial Register Endpoint (/auth/register/initial)', function() {
		var existingRegistration = {
			username: 'initial-existing',
			email: 'test.initial@12150w.com'
		};
		
		before('Add an existing registration', function(done) {
			testAPI.apiRequest({
				uri: '/auth/register/initial',
				method: 'POST',
				body: existingRegistration
			}).then(function() {
				done();
			}).done()
		});
		
		it('Fails with a duplicate username', function(done) {
			testAPI.request({
				uri: '/auth/register/initial',
				method: 'POST',
				body: existingRegistration
			}).then(function(res) {
				assert.equal(res.message.statusCode, 400);
			}).then(function() {
				done();
			}).done();
		});
		it('Sends the registration code to the registered email');
	});
});