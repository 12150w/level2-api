/* 
 * Tests the root service at /
 * 
 */
var level2 = require('../..'),
	dev = require('../../dev-modules'),
	express = require('express'),
	request = require('request'),
	assert = require('assert');

describe('Root Service', function() {
	var testAPI = new dev.TestAPI('Test API');
	
	before('Start the api', function(done) {
		testAPI.start().then(function() {
			done();
		}).done();
	});
	after('Stop the test api', function(done) {
		testAPI.stop().then(function() {
			done();
		}).done();
	});
	
	describe('Root Endpoint (/)', function() {
		it('Returns the api name', function(done) {
			testAPI.request({uri: '/', method: 'GET'}).then(function(res) {
				assert.equal(res.message.statusCode, 200, '200 should be returned');
				assert.equal(res.body.name, 'Test API', 'The name should match the api instance name');
				done();
			}).done();
		});
	});
});