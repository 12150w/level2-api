/* 
 * Tests the DataSession class
 * 
 */
var level2 = require('..'),
	q = require('q'),
	assert = require('assert');

describe('DataSession', function() {
	var testUser = {id: 1, groups: ['test']},
		otherUser = {id: 2, groups: []},
		testSrc, testAdapter, testSession, otherSession;
	
	beforeEach('Create a test session', function() {
		testAdapter = new level2.DataAdapter();
		testSrc = new level2.DataSource(testAdapter);
		testSession = testSrc.createSession(testUser);
		otherSession = testSrc.createSession(otherUser);
	});
	
	describe('#create', function() {
		var testData;
		beforeEach(function() {
			testData = {a: 1, b: 2};
		});
		
		it('Resolves to the inserted data with generated id', function(done) {
			testSession.create('test', testData).then(function(inserted) {
				assert.ok(inserted.id != null, 'id was not set after insert');
				done();
			}).done();
		});
		
		it('Calls addSharingBefore and addSharingAfter on the adapter', function(done) {
			var sharingBeforeCalled = false,
				sharingAfterCalled = false;
			testAdapter.addSharingBefore = function(typeName, options, data) {
				sharingBeforeCalled = true;
				return q.fcall(function() { return data; });
			};
			testAdapter.addSharingAfter = function(typeName, options, data) {
				sharingAfterCalled = true;
				return q.fcall(function() {});
			};
			
			testSession.create('test', testData).then(function() {
				assert.ok(sharingBeforeCalled === true, 'addSharingBefore was not called');
				assert.ok(sharingAfterCalled === true, 'addSharingAfter was not called');
				done();
			}).done();
		});
	});
	
	describe('#read', function() {
		var inserted;
		beforeEach('Insert a record to query', function(done) {
			testSession.create('test', {a: 1, b: 2}).then(function(newRecord) {
				inserted = newRecord;
				done();
			}).done();
		});
		
		it('Queries matching records', function(done) {
			testSession.read('test', {a: 1, b: 2}).then(function(res) {
				assert.ok(res.length === 1, 'No record was queried');
				assert.ok(res[0].id === inserted.id, 'The incorrect record was queried');
				
				done();
			}).done();
		});
		
		it('Does not query records that are not matching', function(done) {
			testSession.read('test', {a: 1, b: 1}).then(function(res) {
				assert.ok(res.length === 0, 'A record was queried when it should not have been');
				
				done();
			}).done();
		});
		
		it('Calls the adapter add filter security', function(done) {
			var calledAddSharing = false;
			testAdapter.addSharingToQuery = function(typeName, query, user) {
				calledAddSharing = true;
				return q.fcall(function() { return query; });
			};
			
			testSession.read('test', {}).then(function() {
				assert.ok(calledAddSharing === true);
				done();
			}).done();
		});
		
		it('Does not return records that the user has no access to', function(done) {
			otherSession.read('test', {a: 1, b: 2}).then(function(found) {
				assert.ok(found.length === 0, 'The record was queried despite the security on the record');
			}).then(function() {
				done();
			}).done();
		});
	});
	
	describe('#readGeneric', function() {
		var inserted;
		beforeEach('Insert a record to query', function(done) {
			testSession.create('test', {a: 1, b: 2}).then(function(newRecord) {
				inserted = newRecord;
				done();
			}).done();
		});
		
		it('Queries matching records', function(done) {
			var matchQuery = new level2.Query('test', {a: 1, b: 2});
			testSession.readGeneric(matchQuery).then(function(res) {
				assert.ok(res.length === 1, 'No record was queried');
				assert.ok(res[0].id === inserted.id, 'The incorrect record was queried');
				
				done();
			}).done();
		});
		
		it('Does not query records that are not matching', function(done) {
			var diffQuery = new level2.Query('test', {a: 1, b: 1});
			testSession.readGeneric(diffQuery).then(function(res) {
				assert.ok(res.length === 0, 'A record was queried when it should not have been');
				
				done();
			}).done();
		});
		
		it('Rejects when a non Query instance is passed as the query', function(done) {
			testSession.readGeneric({}).then(function() {
				done(new Error('Query should not have resolved'));
			}, function() {
				done();
			}).done();
		});
		
		it('Allows filtering based on the greaterThan in the options', function(done) {
			var ltQuery = new level2.Query('test', {}, {greaterThan: {a: 0}});
			testSession.readGeneric(ltQuery).then(function(found) {
				assert.ok(found.length === 1, 'The record was not queried');
				
				var notMatchQuery = new level2.Query('test', {}, {greaterThan: {a: 50}});
				return testSession.readGeneric(notMatchQuery);
			}).then(function(found) {
				assert.ok(found.length === 0, 'The record should not have been queried');
			}).then(function() {
				done();
			}).done();
		});
	});
	
	describe('#update', function() {
		var inserted;
		beforeEach('Insert a record to query', function(done) {
			testSession.create('test', {a: 1, b: 2}).then(function(newRecord) {
				inserted = newRecord;
				done();
			}).done();
		});
		
		it('Updates the record', function(done) {
			testSession.update('test', inserted.id, {a: 3, c: 4}).then(function() {
				return testSession.read('test', {id: inserted.id});
			}).then(function(readIn) {
				assert.ok(readIn[0].a === 3);
				assert.ok(readIn[0].c === 4);
				done();
			}).done();
		});
		
		it('Does not allow updates without permission', function(done) {
			var otherSession = testSrc.createSession({id: 2, groups: []});
			otherSession.update('test', inserted.id, {a: 33}).then(function() {
				done(new Error('Update should not have succeeded'));
			}, function(err) {
				assert.ok(err instanceof level2.Level2Exception);
				done();
			});
		});
	});
	
	describe('#destroy', function() {
		var inserted;
		beforeEach('Insert a record to query', function(done) {
			testSession.create('test', {a: 1, b: 2}).then(function(newRecord) {
				inserted = newRecord;
				done();
			}).done();
		});
		
		it('Removes the record from the store', function(done) {
			testSession.destroy('test', inserted.id).then(function() {
				return testSession.readGeneric(new level2.Query(
					'test', {id: inserted.id}, {limit: 1}
				));
			}).then(function(results) {
				assert(results.length === 0);
				done();
			}).done();
		});
		
		it('Prevents deletion from a user without permission', function(done) {
			var otherSession = testSrc.createSession({id: 2, grups: []});
			otherSession.destroy('test', inserted.id).then(function() {
				done(new Error('Should not have allowed the delete'));
			}, function(err) {
				assert.ok(err instanceof level2.Level2Exception);
				done();
			});
		});
	});
	
	describe('#describe', function() {
		beforeEach('Insert a record to set up a type', function(done) {
			testSession.create('some-type', {a: 1, b: 2}).then(function() {
				done();
			}).done();
		});
		
		it('Resolves with the adapter types', function(done) {
			testSession.describe().then(function(description) {
				assert.ok(description.hasOwnProperty('types'), 'No types found in description');
				assert.ok(description.types.indexOf('some-type') !== -1, 'The types were not correct');
				
				done();
			}).done();
		});
	});
});