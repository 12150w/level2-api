/* 
 * SharingOption
 * 
 */
var Class = require('./base/class');

module.exports = Class.extend({
	init: function(type, accessLevel, target) {
		this.type = type;
		this.accessLevel = accessLevel;
		this.target = target;
	}
});

module.exports.type = { USER: 1, GROUP: 2, EVERYONE: 3 };
module.exports.level = { READ: 1, WRITE: 2, DELETE: 3, ADMIN: 4 };