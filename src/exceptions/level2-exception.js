/* 
 * Level2Exception
 * 
 */

module.exports = function Level2Exception(code, msg) {
	Error.captureStackTrace(this, this.constructor);
	this.name = this.constructor.name;
	this.message = msg + ' (' + code + ')';
	this.extra = code;
};
module.exports.prototype = new Error();