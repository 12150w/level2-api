/* 
 * Endpoint Exception
 *
 */

module.exports = function EndpointException(message, statusCode, code) {
	Error.call(this);
	Error.captureStackTrace(this, this.constructor);
	this.name = this.constructor.name;
	
	this.message = message;
	this.statusCode = statusCode || null;
	this.code = code || null;
};
module.exports.prototype = new Error();