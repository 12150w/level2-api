/* 
 * APIException
 * 
 */

module.exports = function APIException(statusCode, info) {
	Error.captureStackTrace(this, this.constructor);
	this.name = this.constructor.name;
	this.info = info;
	
	this.body = info;
	this.statusCode = statusCode;
	
	// Format the message
	this.message = statusCode + ' ' + info.error;
	if(!!info.code) {
		this.message += ' (' + info.code + ')';
	}
	if(!!info.trace) {
		this.stack = info.trace;
	}
};
module.exports.prototype = new Error();