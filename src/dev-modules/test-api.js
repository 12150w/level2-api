/* 
 * TestAPI
 * 
 */
var API = require('../api'),
	DataSource = require('../data-source'),
	DataAdapter = require('../data-adapter'),
	AuthProvider = require('../auth-provider'),
	APIException = require('../exceptions/api-exception'),
	q = require('q'),
	express = require('express'),
	request = require('request'),
	os = require('os'),
	path = require('path'),
	crypto = require('crypto'),
	net = require('net');

module.exports = API.extend({
	init: function() {
		this._super.apply(this, arguments);
		
		this.socketPath = null;
		this.store = null;
		
		// Set up the session
		this.setAuthProvider(new AuthProvider());
		this._preLoadAdapter = new DataAdapter();
		this._authDataSession = (new DataSource(this._preLoadAdapter)).createSession({id: null, groups: []});
		
		this._server = null;
		this._testSID = null;
		this._secureUser = null;
		this._dataSession = null;
		this._testPassword = 's02masdf@#asla';
	},
	
	start: function(baseUrl) {
		var self = this;
		
		// Set the data source if blank
		if(self.store === null) self.store = new DataSource(this._preLoadAdapter);
		if(self.authProvider === null) self.authProvider = new AuthProvider();
		self._dataSession = self.store.createSession({id: null, groups: []});
		
		return q.nfcall(crypto.pseudoRandomBytes, 3).then(function(rawKey) { // Generate a socket path
			//TODO: set this up so that it works using windows named pipes as well
			if(!!self.socketPath) {
				return;
			} else {
				if(os.platform() === 'win32') self.socketPath = '\\\\?\\pipe';
				else self.socketPath = os.tmpdir();
				
				self.socketPath = path.join(self.socketPath, 'test-api-' + rawKey.toString('hex').toUpperCase() + '.sock');
			}
			
		}).then(function() { // Start the express app
			var startDefer = q.defer();
			
			// Run the app
			self._app = express();
			self.run(self._app, baseUrl);
			
			self._server = self._app.listen(self.socketPath, function(err) {
				if(err) startDefer.reject(err);
				else startDefer.resolve();
			});
			
			return startDefer.promise;
		});
	},
	
	stop: function() {
		return q.ninvoke(this._server, 'close');
	},
	
	request: function(options) {
		options = options || {};
		
		options.baseUrl = 'http://unix:' + this.socketPath + ':/';
		options.json = true;
		
		return q.nfcall(request, options).then(function(results) {
			return {
				message: results[0],
				body: results[1]
			};
		});
	},
	
	apiRequest: function(options) {
		return this.request(options).then(function(res) {
			if(res.message.statusCode < 400) return res;
			throw new APIException(res.message.statusCode, res.body);
		});
	},
	
	secureRequest: function(options) {
		var self = this;
		
		return this._getApiSID().then(function(sid) {
			options.headers = options.headers || {};
			options.headers[self.sessionKey] = sid;
			return self.apiRequest(options);
		});
	},
	
	getSecureSession: function() {
		var self = this;
		
		return self._getSecureUser().then(function(apiUser) {
			return self.store.createSession({id: apiUser.id, groups: []});
		});
	},
	
	_getApiSID: function() {
		var self = this;
		
		// Check for cached SID
		if(self._testSID !== null) return q.fcall(function() { return self._testSID; });
		
		// Create a new logged in user
		return self._getSecureUser().then(function(user) {
			return self.authProvider.finalRegister(user.registrationKey, self._testPassword, self._dataSession);
		}).then(function() {
			return self.authProvider.login({username: 'testapi', password: self._testPassword}, self._dataSession);
		}).then(function(sid) {
			self._testSID = sid;
			return sid;
		});
	},
	
	_getSecureUser: function() {
		var self = this;
		
		if(self._secureUser !== null) return q.fcall(function() { return self._secureUser; });
		
		var initialPromise = self.authProvider.initialRegister({username: 'testapi', email: 'test@12150w.com'}, self._authDataSession);
		return initialPromise.then(function(user) {
			self._secureUser = user;
			return user;
		});
	}
});