# Query
The query class represents a generic query that can be used with any data adapter.
This is used for internal data access so that end user implementation is not limited to certain databases.
Unless database flexibility is useful we suggest using queries specific to your application.

:warning: The internal data access needs are not very complex therefore generic queries don't support much in terms of specific database features (such as joins or complex filters)

## Methods

### init(typeName, filter, [options])
Constructs a new query

* **typeName** `String`: The type name that should be queried
* **filter** `Object`: Key value pairs that must match queried records
* **options** `Object`: Options for this query

:information_source: The current supported options are as follows:

* **limit** `Number`: The maximum number of records to be queried
* **disableSecurity** `Boolean`: Set to `true` and the data source will not enforce any record security **USE WITH CAUTION**
* **greaterThan** `Object`: Key value pairs for filtering where value on record is greater than the value on the filter

### getTypeName() - `String`
Gets the type name

:arrow_right: Returns the type name for this query

### getFilter() - `Object`
Gets the raw filter for this query

:arrow_right: Returns the filter that this query was constructed with

### getOptions() - `Object`
Gets the options for this filter

:arrow_right: Returns the options for this filter (or an empty object if the options were `null` on init)