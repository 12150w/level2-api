# Endpoint
An endpoint handles requests sent to a certain URL.
Endpoints are always contained in [Service](docs/class-service.markdown) instances.


## Methods

### init([httpMethod], requestHandler)
Constructs a new endpoint that will use the handler for processing requests

* **[httpMethod]** `String`: The optional HTTP method (GET, POST, PUT, ...) that this endpoint will be registered for (if blank the endpint will be registerd for all methods)
* **requestHandler** `Function`: The handler function that will be called to process each request with the following arguments:
    * **req** `Express.Request`: The request to process
    * **res** `Express.Response`: The Express response
    * [**next**] `Function`: Optional next function from Express, call it to look for other matching URLs

:information_source: The request handler function is called in the context of the endpoint instance (which can be accessed via `this`)

### processRequest(req, res, next)
Handles a request.
This is called by the service to pass the request on to the endpoint for processing.

* **req** `Express.Request`: The request to process
* **res** `Express.Response`: The request's response
* **next** `Function`: The Express next function

:information_source: The default implementation calls the request handler

### preProcess(req, res, next) - `Promise`
Run before the request is processed (before `processRequest` is called) to determine if the request should be processed further.

* **req** `Express.Request`: The request to process
* **res** `Express.Response`: The request's response
* **next** `Function`: The Express next function

:arrow_right: Returns a promise that indicates if the request should be further processed: if resolved then the **request is processed**, if rejected **request processing is cancelled**


## Properties

### api `API`
The API that this endpoint is a member of

:warning: This attribute is `null` until the endpoint's serive is registered

### fullPath `String`
The full path that this endpoint is accessible at (relative to the API)

:warning: This attribute is `null` until the endpoint's serive is registered

### service `Service`
The service this endpoint is registered under

:warning: This attribute is `null` until the endpoint's serive is registered