# DataSource
The DataSource is the main interface that is used by an API to access data.
The role of the DataSource is to take requests from the API and manage the exchange between the DataAdapter to process the request.

## Methods

### init(adapters...)
Creates a DataSource instance for the given DataAdapter instances

* **adapters...** `DataAdapter`: List of adapters for this DataSource

### createSession(user, [adapterKey]) - `DataSession`
Creates a data session in the context of the passed in user

* **user** `Object`: The user identity that will be used by the adapter to securely access the database
* **adapterKey** `Number`: Optional index to specify which adapter is used

:arrow_right: Returns a `DataSession` instance