# SharingOption
The SharingOption class is used to hold information for a sharing rule related to a record.
For details on the sharing process see the [Security Levels](secure-levels) document.


## Methods

### init(type, accessLevel, target)
Creates a new SharingOption with the given values

* **type** `SharingOption.type`: The type of this option
* **accessLevel** `SharingOption.level`: The access level for this option
* **target** `String | Number | null`: The id of the user or group name (depends on the option's type)


## Attributes

### type - `SharingOption.type`

### accessLevel - `SharingOption.level`

### target - `String | Number | null`

:information_source: The type of this attribute depends on the type of this sharing option


## Static Attributes

### type - `Object`
Contains the valid sharing types which are:

* **USER**: Shares with a specific user
* **GROUP**: Shares with a specific group
* **EVERYONE**: Shares with every user

### level - `Object`
Contains the valid sharing levels which are:

* **READ**: Read only access
* **WRITE**: Read and write access (no sharing)
* **DELETE**: Read, write, and delete access (no sharing)
* **ADMIN**: Read, write, delete, and sharing access