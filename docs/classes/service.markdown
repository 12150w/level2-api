# Service
The Service class provides a way to define new services used by the level2 API.
It is used to define all the built in services and user defined services.


## Methods

### init()
Create a new service at the given path


## Properties

### endpoints `Object - Endpoint | Function`
A key value pair between paths and an Express middle-ware functions.
For example if we wanted to define an endpoint at '/test-service/test-endpoint' we could use:

```javascript
level2.Service.extend({
    path: '/test-service',
    endpoints: {
        'test-endpoint': function(req, res, next) {
            // Do request processing here
        }
    }
});
```

If the value is a function then an Endpoint instance is created using that function.

:information_source: The function is run in the context of the endpoint