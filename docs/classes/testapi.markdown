TestAPI - `API`
================================================================================================
Provides an easy way to create an API for testing.
The testing API is accessed via a UNIX socket instead of binding to a port which may be used.

:information_source: This class extends the base API class

:warning: Do not create an Express app and use `run()` to use this API, instead use the `start()` and `stop()` methods that do this for you.


Methods
------------------------------------------------------------------------------------------------

### start([baseUrl]) - `Promise`
Starts the test API.

:arrow_right: Returns a promise that resolves once the API has been started

* **baseUrl** `String`: Base url for the API

### stop() - `Promise`
Stops the test API.

:arrow_right: Returns a promise that resolves once the API has been fully stopped.

### request(options) - `Promise - Object`
A customized request function (using the `request` module) that sets the defaults for connecting to the test API.

* **options** `Object`: The options that are passed to the request module

:arrow_right: Returns a promise for an object that contains the following:

* **message** `http.IncomingMessage`: The http message from request
* **body** `String | Buffer | Object`: The request body, the actual type depends on the options sent to request

### apiRequest(options) - `Promise - Object`
The same as `request()` except that HTTP errors result in the promise being rejected

* **options** `Object`: The options that are passed to the request module

:arrow_right: Returns a promise for an object that contains the following:

* **message** `http.IncomingMessage`: The http message from request
* **body** `String | Buffer | Object`: The request body, the actual type depends on the options sent to request

### secureRequest(options) - `Promise - Object`
The same as `apiRequest()` except that a valid session Id is attached to the request so secure endpoints may be accessed

* **options** `Object`: The options that are passed to the request module

:arrow_right: Returns a promise for an object that contains the following:

* **message** `http.IncomingMessage`: The http message from request
* **body** `String | Buffer | Object`: The request body, the actual type depends on the options sent to request

### getSecureSession() `Promise - DataSession`
Returns a promise for a data session as the same user that `secureRequest` uses to make secure requests

:arrow_right: Returns a promise for the data session as the secure user


Properties
------------------------------------------------------------------------------------------------

### socketPath `String`
The path to the socket for this API.

:warning: This is not set until after `start()` resolves