# AuthProvider
The AuthProvider class provides an interface between a database of users and sessions and the API (and Services within that API).
The base implementation stores authentication information in the provided data source.
Other implementations may provide access the 3rd party identity providers for a single sign on like ability.


## Methods

### init()
Creates a base auth provider instance

:warning: Other auth provider implementations may change the init arguments

### login(identity, dataSession) - `Promise - String`
Attempts to log in the user specified by identity.

* **identity** `Object`: A plain object that identifies the user
* **dataSession** `DataSession`: Session connection to database

:arrow_right: Returns a Promise that resolves with the session id for that user if the identity is valid

:information_source: The default implementation expects `username` and `password` in the identity

```javascript
{
    username: "some-username",
    password: "user-password"
}
```

### logout(sessionId, dataSession) - `Promise`
Attempts to end an active session that is identified by the session id argument

* **sessionId** `String`: The id of the session to end
* **dataSession** `DataSession`: Session connection to database

:arrow_right: Returns a Promise that is resolved if the session id is valid and an active session is ended (this promise resolves if the session has already ended as well)

### validate(sessionId, dataSession) - `Promise - Object`
Tries to find a valid and open session with a matching session id

* **sessionId** `String`: The id of the session that should be validated
* **dataSession** `DataSession`: Session connection to database

:arrow_right: Returns a promise that resolves with the **public** user information (public meaning it won't contain confidential information such as the password)

### initialRegister(userInfo, dataSession) - `Promise`
Registers a new, in-active, user who's email needs to be verified

* **userInfo** `Object`: A plain object containing all the necessary information to pre-register a user with this specific auth provider
* **dataSession** `DataSession`: Session connection to database

:arrow_right: Returns a promise that resolves when the user is pre=registered

:information_source: If a specific auth provider implementation only requires an initial registration then the returned promise with resolve when the user is fully registered

:information_source: The default implementation expects the following information in `userInfo`:

```javascript
{
    username: "new-username",
    email: "email@sample.com"
}
```

### finalRegister(registrationKey, password, dataSession) - `Promise`
Finalizes an initial registration thus validating the email

* **registrationKey** `String`: The unique registration identifier contained in the email sent during initial registration
* **password** `String`: Password to set for the user (must be at least 7 characters long)
* **dataSession** `DataSession`: Session connection to database

:arrow_right: Returns a promise that is resolved when the registration completes

### getPublicUser() - `Object`
Returns the user identity for public sessions

:arrow_right: Returns the identity for public sessions


## Properties

### userType `String`
The type name that is used to store users

### sessionType `String`
The type name that is used to store sessions

### sessionPeriod `Number`
The number of seconds a session is valid for since it was last used