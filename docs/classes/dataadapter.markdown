# DataAdapter
The DataAdapter is used by the DataSource to persist and retrieve data.
Security is implemented by the DataSource calling choice methods (described below) on the DataAdapter.

The default implementation of the DataAdapter stores all data in memory and is reset when the application starts.
The DataAdapter is *intended to be extended* to provide support for various databases and storage methods.

:warning: This documents the default DataAdapter and its API. Other implementations of the DataAdapter my have extended APIs.



## Base Methods
These are the methods that allow CRUD operations with the data store.

### init([name])
Constructs a new DataAdapter

* **name** `String`: The name of the adapter (default `null`)

### commit(typeName, data) - `Promise - Object`
Inserts data into the store for the type

* **typeName** `String`: Name of the type of data this is
* **data** `Object`: The data to be inserted by the adapter

:arrow_right: Returns a Promise for the saved version of data (with any generated values)

### extract(typeName, query) - `Promise - Array`
Retrieves data from the store.

* **typeName** `String`: The type that should be extracted from
* **query** `Object | String | Number`: If String or Number then returns records that have that as an id, if Object then finds records that meet the query parameters

:arrow_right: Returns a Promise for an Array of the records found or an empty Array if no records match the query

:information_source: `query` could be any type, it depends on the adapter (for example a SQL adapter may use a String or Query class for `query`)

### update(typeName, data) - `Promise - Object`
Updates the passed in data and uses `data.id` to locate the existing record.
If no record is found then an error is thrown.

* **typeName** `String`: Name of the type of data that is being updated
* **data** `Object`: Object with values to be updated (must contain a valid `id` attribute)

:arrow_right: Returns a Promise for an Object with the updated data (including any calculated fields such as last updated)

### destroy(typeName, id) - `Promise`
Destroys the record of type `typeName` with a the same `id`.

* **typeName** `String`: Name of the type that needs to be deleted
* **id** `String | Number`: Id of the record to delete

:arrow_right: Returns a Promise that is resolved when the record is fully deleted

:warning: This will destroy the record **irreversibly** (not just mark it as deleted)

### convertQuery(query) - `Object | String`
Converts a generic query (`Query` instance) into a query specific to this adapter

* **query** `Query`: The query instance that needs to be converted

:arrow_right: Returns a query that can be passed to this adapter's extract method

### getTypes() - `Promise - Array`
Gets a list of types for this adapter.

:arrow_right: Returns a promise for an array of strings which are the type names



## Security Methods
These methods are called by the data store to implement security using this adapter.

### addSharingBefore(typeName, options, data) - `Promise - Object`
Allows adding sharing options to a new record before it is inserted.
The default implementation adds the options to the `sharing` attribute on the record.

* **typeName** `String`: Name of the data type being created
* **options** `Array`: Array of `SharingOption` instances to be added to the data store
* **data** `Object`: The data that will be inserted

:arrow_right: Returns a Promise that resolves to the data with the options added

:information_source: `data.id` will **not** be set at this point

:warning: Either this method or `addSharingAfter` must be implemented

### addSharingAfter(typeName, options, data) - `Promise`
Allows adding sharing options to a new record after it has been inserted.
The default implementation returns a Promise that does nothing.

* **typeName** `String`: Name of the data type that was inserted
* **options** `Array`: Array of `SharingOption` instances to be added to the data store
* **data** `Object`: The data that was inserted

:arrow_right: Returns a Promise this is resolved when the options have been added

:information_source: `data.id` and any other calculated field will be populated at this point

:warning: Either is method or `addSharingBefore` must be implemented

### addSharingToQuery(typeName, query, user) - `Promise - Object`
Adds sharing filters to a query before it is run.

* **typeName** `String`: The type that is going to be queried
* **query** `Object | String`: The query that was passed to the adapter's query function
* **user** `Object`: The user that is running this query which includes the following attributes:

:arrow_right: Returns a promise that resolves with the query that has sharing filters added

### getSecurityLevel(record, user) - `Promise - Number`
Gets the highest security level for a certain record that the user has

* **record** `Object`: The record to get the security level from
* **user** `Object`: User identity

:arrow_right: Returns a promise for the highest security level



## User Identities
The user identity contents depends on the auth provider that is in use.
At a bear minimum user identities must contain the following:

* **id** `String | Number`: The id of the user
* **groups** `Array - String`: An array of group names that the user is a member of