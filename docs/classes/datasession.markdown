# DataSession
Provides user context to the database and provides the interface that services use to access the database.

## Methods

### init(user, adapter)
Constructs a new data session

* **user** `Object`: User identity passed to the adapter to secure database transactions
* **adapter** `DataAdapter`: Adapter to access the database with

### create(typeName, data) - `Promise - Object`
Inserts a new record of a certain type

* **typeName** `String`: Name of the type of data being inserted, its meaning depends on the adapter being used
* **data** `Object`: The data that will be inserted

:arrow_right: Returns a Promise for the data that was inserted, including any generated attributes (such as an `id`)

### read(typeName, query, [options]) - `Promise - Array`
Queries a specific type

* **typeName** `String`: The type name to query
* **query** `String | Object`: Query that is passed to the adapter
* **options** `Object`: Optional options that support the following properties
    * **disableSecurity** `Boolean`: Set to true to disable security checks

:arrow_right: Returns a promise that resolves with an array of queried records

:information_source: When readGeneric is called the options from the `Query` instance are passed to the options here

### readGeneric(query) - `Promise - Array`
Runs a generic query

* **query** `Query`: The query instance that will be used to find matching records

:arrow_right: Returns a promise that resolves with an array of the results

### update(typeName, id, data) - `Promise - Object`
Updates the record of a certain type with a matching id

* **typeName** `String`: Type that will be updated
* **id** `String | Number`: Id of the record to update
* **data** `Object`: Data to update on the record

:arrow_right: Returns a promise that resolves with the updated version of the record (including any formula fields)

### delete(typeName, id) - `Promise`
Same as destroy

:warning: In future versions this function will only mark the record as deleted

### destroy(typeName, id) - `Promise`
Irreversibly deletes a record

* **typeName** `String`: Name of the type that will be destroyed
* **id** `String | Number`: Id of the record to destroy

:arrow_right: Returns a promise that resolves when the record is successfully destroy

### describe() - `Promise - Object`
Returns a promise for an object describing this data session and its adapter.

:arrow_right: Returns a promise that resolves with an object containing the following:

* **types** `Array - String`: The types currently available in the session's adapter


## Properties

### isPublic `Boolean`
Is true when this is a public session (no authorized user) otherwise is false