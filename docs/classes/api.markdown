# API
The API class defines a level2 API and interfaces a user API to an express server.

:information_source: The latest version of level2 is designed to work with Express **version 4**


## Methods

### init(apiName)
Creates the API as defined in the definition function

* **apiName** `String`: The name of the API

### run(app, [basePath])
Adds the API middle-ware to the passed in Express app.
The app will be run at the root of the application so it is suggested to use a router to give your API a namespace.

* **app** `Express.Application | Express.Router`: Application or router to add the API to
* **basePath** `String`: Optional base path to run the API off of

:arrow_right: Returns the API instance

### setSource(dataSource)
Sets the DataSource for this API

* **dataSource** `DataSource`: The data source instance to add to the API

:arrow_right: Returns the API instance

### setAuthProvider(provider)
Sets the AuthProvider for this API

* **provider** `AuthProvider`: The auth provider instance

:arrow_right: Returns the API instance

### registerService(basePath, service)
Adds the service at the given base path

* **basePath** `String`: The base path to run the service at
* **service** `Service`: The service instance to register

:arrow_right: Returns the API instance


## Properties

### store `DataSource`
The data source for this API.

:warning: Do **not** change this attribute directly, instead use `setSource`

### authProvider `AuthProvider`
The auth provider for this API.

:warning: Do **not** change this attribute directly, instead use `setAuthProvider`

### basePath `String`
The root path of the API.
Defaults to `'/'`.

:warning: This is not set until **after** `run()` is called