SecureEndpoint - `Endpoint`
================================================================================================
The secure endpoint class extends the endpoint class and validates the session
	before running the endpoint handler.