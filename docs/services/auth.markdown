Authentication Service
================================================================================================
Allows managing users, session management, and registration.
This is the only way to create new users and to manage users via HTTP.


Endpoints
------------------------------------------------------------------------------------------------

### POST `/auth/register/initial`
POST to this endpoint to initial register a user.
An initially registered user can not log in until their email is verified
	and their final registration is complete.
The request body must contain:

```json
{
    "username": "<NEW USERNAME>",
    "email": "<NEW EMAIL>"
}
```

* **username** `String`: Username of the user to be registered
* **email** `String`: Valid email of the user to be registered

A success HTTP status code indicates that the initial registration was successful.

:warning: If there is any existing user with a matching username or password an error is returned

:information_source: A link for final user registration 


### POST `/auth/register/final`
Use this endpoint to do the final user registration and set the user password.
The request body will contain:

```json
{
    "key": "<REGISTRATION KEY>",
    "password": "<NEW PASSWORD>"
}
```

A successful HTTP status code indicates that the final registration was successful.


### POST `/register/invite`
This endpoint works similarily to initial register endpoint except that it may only
	be accessed by admin users.
To send an invitation to a new user POST the following:

```json
{
    "username": "<INVITED USER USERNAME>",
    "email": "<INVITED USER EMAIL>"
}
```

* **username** `String`: Username of the user to be invited
* **email** `String`: Valid email of the user to be ivited

A successful HTTP status code indicates that the invitation was sent

:warning: Only admin users can use this endpoint


### POST `/login`
POST to this endpoint to log in a user and get a valid session id.
The body must contain:

* **username** `String`: Username of the user to log in
* **password** `String`: Password of the user to log in

If the login is valid a successful HTTP status code will be sent and the response will be:

```json
{
    "sid": "<USER SESSION ID>"
}
```

:warning: If a login call is sent for a user that currently has a valid session, that same session Id will be returned


### POST `/logout`
POST to this endpoint to close a valid session and log a user out.
The post body must contain:

* **sid** `String`: Session Id of the session to close

If the session is found a successful http status code is returned

:information_source: if the sid belongs to a session that is already closed the endpoint still returns success