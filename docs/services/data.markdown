Data Service
================================================================================================
The data service provides access to the data store.
This is a generic data service that may work with generic client side adapters.

:information_source: If you have complex data structures (which is likely)
	then please create a specialized service for each data type

:warning: If no data source is set for the API all endpoints in the data service
	will return a 500 error


Endpoints
------------------------------------------------------------------------------------------------


### GET `/data/describe`
Returns a list of valid types in the data store.

```json
{
    "types": [
        "type_a", "type_b", "type_c"
    ]
}
```

* **types** `Array - String`: List of types in the store


### POST `/data/records/<TYPE NAME>`
Creates a new record with the type name specified in the URL.
The inserted record (including Id and any other generated field) is returned.

```json
{
    "id": 1,
    "sample-field": "sample-value"
}
```


### GET `/data/records/<TYPE NAME>`
Returns a page of records.

```json
{
    "records": [
        {"id": 1},
        {"id": 2}
    ],
    "hasMore": true
}
```

* **records** `Array - Object`: List of records for this page
* **hasMore** `Boolean`: True if more pages of records are following this page

:information_source: Each items in records contains all the fields for the record