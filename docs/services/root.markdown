Root Service
================================================================================================
The root service describes the API.


Endpoints
------------------------------------------------------------------------------------------------

### GET `/`
Returns the description about this API

```json
{
    "name": "<API Name>"
}
```