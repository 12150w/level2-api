# Level2 API
The Level2 API provides a standard for creating simple web API's

## Contents

### API Reference
The API Reference documents the specification and capability of the Level2 API.
This *is not* instructions on how to use the Level2 API (for that see the Usage Guide).

| Class                                                   | Description                                                         |
|---------------------------------------------------------|---------------------------------------------------------------------|
| [API](docs/classes/api.markdown)                        | Base API class for creating level2 APIs                             |
| [AuthProvider](docs/classes/authprovider.markdown)      | Base class and implementation for managing users and sessions       |
| [DataAdapter](docs/classes/dataadapter.markdown)        | Provides an interface for a Level2 DataSource to persist data       |
| [DataSession](docs/classes/datasession.markdown)        | Provides context of a user to the data source                       |
| [DataSource](docs/classes/datasource.markdown)          | Defines a source of information used by the application             |
| [Endpoint](docs/classes/endpoint.markdown)              | Handles requests for a certain URL                                  |
| [Query](docs/classes/query.markdown)                    | Generic query object use when the database must be generic          |
| [SecureEndpoint](docs/classes/secure-endpoint.markdown) | Verifies that the client is logged in before running the endpoint   |
| [Service](docs/classes/service.markdown)                | Base service class for defining new API services                    |
| [SharingOption](docs/classes/sharingoption.markdown)    | Defines a single sharing option or rule for a record                |

### Testing Reference
These modules are only available during development since they depend on the `devDependencies`.
None of these modules are exported via the main `level2` object (defined in [index.js](src/index.js)).

| Class                                               | Description                                                         |
|-----------------------------------------------------|---------------------------------------------------------------------|
| [TestAPI](docs/classes/testapi.markdown)            | An API for testing (used in the test suite)                         |

### HTTP API
These documents describe the HTTP interface to an API instance

#### Services

| Service                                                | Endpoint            | Description                                                             |
|--------------------------------------------------------|---------------------|-------------------------------------------------------------------------|
| [Root Service](docs/services/root.markdown)            | `/`                 | Describes the API                                                       |
| [Data Service](docs/services/data.markdown)            | `/data`             | Provides access to the data store                                       |
| [Authentication Service](docs/services/auth.markdown)  | `/auth`             | Manage user and security features using this service                    |

### Other Resources
These are documents on items that are not explicit class references

| Topic                                                       | Description                                                      |
|-------------------------------------------------------------|------------------------------------------------------------------|
| [Exceptions](docs/other/exceptions.markdown)                | Information on the Exceptions thrown by this module              |
| [Security Levels](docs/other/secure-levels.markdown)        | Describes how records are secured and the sharing options        |
| [Internal Data Model](docs/other/data-model.markdown)       | Information on the internal data schema                          |