# Security Levels
Records stored using the level2 api have security and sharing enabled.
This document logically describes how records are secured and shared.
The actual implementation of how records are secured is dependent upon the data adapter.

## Record Sharing Attribute
Each record contains a list of sharing options that describe with whom or what the record is shared with as well as the permissions the target has for the record.
Where or how the sharing options are saved is dependent upon the data adapter.
For example a MongoDB adapter may store the sharing options as a sub-document on the record.
A SQL adapter may store the options on a related record to the table.

### Sharing Option
A sharing option record contains the following attributes:

* **type**: Specifies whether this option applies to a user, group, or everyone
* **target**: Identifies who or what this option applies to (if this option is for everyone then the target should be blank)
* **accessLevel**: Describes the access level that this option has

### Access Levels
Each sharing option has to identify which access level is granted to the target.
The possible access levels are:

1. **read**: Allows read only access
2. **write**: Allows read and write but **not** sharing the record
3. **delete**: Allows read, write, and delete but **not** sharing the record
4. **admin**: Allows read, write, delete, and sharing the record

:information_source: The higher access levels (4 being the highest) imply the same access as levels lower than it (for example granting delete implies write access)