# Exceptions
These are the exceptions that can be expected to be thrown from the level2 API package.


## Exception Types

### Level2Exception
The base exception for all exceptions thrown by the level2 API

* **code** `String`: The code of this exception
* **msg** `String`: More detailed information about the exception

:information_source: Construct with `new Level2Exception('code', 'msg')`

### APIException
The exception thrown when an API request status code is greater than or equal to 400.
This error has the following attributes:

* **stack** `String`: The server's stack
* **body** `Object`: The request's response
* **statusCode** `Number`: The status code returned from the request