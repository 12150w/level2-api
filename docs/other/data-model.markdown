# Internal Data Model

## Type Prefix
All types in the internal data model are prefixed with `l2_`.

## Types

### l2_user
Stores the user and registration information

* **username** `String`: User's username (`unique`)
* **email** `String`: User's email (`unique`, `email`)
* **registrationKey** `String`: Key used when verifying user's email and null when user is active
* **registrationExpiration** `Date`: Registration key is not valid after this date
* **password** `String`: Encrypted password

### l2_session
Session information for a logged in user

* **user** `String | Number`: Id of the user this session is for
* **key** `String`: Random string used by the browser to identify the session
* **invalidAfter** `Date`: Date that the session is valid until
* **loggedOut** `Boolean`: Flag to tell if the session was logged out manually and did not expire

:information_source: When a user logs out the `invalidAfter` is set to the current time and the `loggedOut` flag is set to true. If you are validating a session then only check the `invalidAfter` date.