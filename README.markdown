# Level2 API
The level2 API provides a framework, based on Express, for creating web applications.
The goal is to provide base functionality, such as users and record level security, with structure as close as possible to Express.

## Testing
The level2 API uses the Mocha testing framework which can be installed using

```
npm install -g mocha
```

:information_source: On some systems this command requires root privileges an must be run using `sudo`

The entire test suite can be run using npm

```
npm test
```

All tests are stored in the [tests](src/tests) directory.

## Other Resources

### [Documentation](docs/README.markdown)
All information about the API can be found here as well as usage guides.
